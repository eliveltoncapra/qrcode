import qrcode
from PIL import Image

def func_qrcode(logo_link, url, nome_arquivo):


    logo = Image.open(logo_link)

    basewidth = 50

    wpercent = (basewidth/float(logo.size[0]))
    hsize = int((float(logo.size[1])*float(wpercent)))
    logo = logo.resize((basewidth, hsize), Image.ANTIALIAS)
    QRcode = qrcode.QRCode(
        error_correction=qrcode.constants.ERROR_CORRECT_H
    )

    QRcode.add_data(url)
    QRcode.make()
    QRcolor = 'Blue'

    QRimg = QRcode.make_image(
        fill_color=QRcolor, back_color="white").convert('RGB')

    pos = ((QRimg.size[0] - logo.size[0]) // 2,
        (QRimg.size[1] - logo.size[1]) // 2)

    QRimg.paste(logo, pos)

    QRimg.save(nome_arquivo)

    print('QR code saved')

func_qrcode('radar.jpg', 'https://www.google.com', 'Teste.jpg')


